
#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import logging
import json
import tornado.websocket
import core_store as ast
import core_rest_base_client as abct
import core_api_parser as cap
import core_exceptions as exp

# monitor_plugin_method = /blabla/<param_1>/<param_2>/some?x=<val1>&y=<val2>
# P&P_NBI_method: /monitor/<plugin_id>/<api_id>/?param_1=<param_1>&param_2=<param_2>&x=<val_1>&y=<val_2>

module_name = "P&P_CORE_ENGINE"
logger = logging.getLogger(module_name)

store = ast.ModuleStorage()


async def register_cpsr_info(info):
    store.store_cpsr_info(info)


async def generate_detailed_slice():
    try:
        slice_info = store.get_slice()
        if not slice_info:
            raise exp.GetInfoError("ERROR No slice found", "SLICE_NOT_FOUND")
        for e in slice_info['elements']:
            e = get_element_info(e['element_id'])
    except KeyError as e:
        logger.error("ERROR in generating detailed slice: " + str(e))
        raise exp.GetInfoError("ERROR in parse slice info", "GET_DETAILED_SLICE_ERROR")
    except exp.GetInfoError:
        raise

    return slice_info


def get_element_info(element_id):
    slice_info = store.get_slice()
    if not slice_info:
        raise exp.GetInfoError("ERROR No slice found", "SLICE_NOT_FOUND")
    try:
        for e in slice_info['elements']:
            if e['element_id'] == element_id:
                for p in e['properties']:
                    p_id = p['property_id']
                    family = p['family']
                    for ap in p['allowed_api']:
                        endpoint, op_par, op_type = generate_nb_endpoint(element_id, p_id, ap['api_id'], family)
                        ap['endpoint'] = endpoint
                        ap['parameters'] = op_par
                        ap['operation_type'] = op_type
                return e
    except (KeyError, exp.GetInfoError):
        raise


def generate_nb_endpoint(element_id, plugin_id, api_id, family):
    module_info = store.get_modules()
    if not module_info:
        raise exp.GetInfoError("ERROR No modules found", "MODULE_NOT_FOUND")
    par_list = []
    try:
        for operation in module_info[plugin_id]['operations']:
            if api_id in operation.keys():
                op_type = operation[api_id]['type']  # FIXME
                op_par = operation[api_id]['parameters']
                for par in op_par:
                    for par_name, par_values in par.items():
                        if par_values['in'] == 'path':
                            par_list.append(par_name)

                endpoint = '/plug-and-play-test/'+family+'/'+element_id+'/'+plugin_id+'/'+api_id+'/'
                if par_list:
                    aux = []
                    endpoint += '?'
                    for par in par_list:
                        aux.append(par+'={'+par+'}')

                    endpoint += '&'.join(aux)
                return endpoint, op_par, op_type
    except KeyError as e:
        logger.error("ERROR in parsing module info: " + str(e))
        raise


def load_slice_model(model):
    elements = {}
    graph = {}
    for e in model['elements']:
        elements[e['element_id']] = e

    graph[model['slice_id']] = model['graph']

    return elements, graph


def store_callback(cb_key, cb):
    store.store_callback(cb_key, cb)


def remove_all_callbacks(id):
    store.delete_all_callback(id)


def remove_callback(cb_key):
    store.delete_callback(cb_key)


def assemble_ws_message(element_id, module_id, api_id, response):
    message = {}

    slice_info = store.get_slice()
    message['slice_id'] = slice_info['slice_id']
    message['tenant_id'] = slice_info['tenant_id']
    message['element_id'] = element_id
    message['module_id'] = module_id
    message['api_id'] = api_id
    message['content'] = response

    return message


async def websocket_routine(ws, element_id, module_id, api_id, params):
    try:
        print("WS ROUTINE", element_id, module_id, api_id, params)
        response = await apply_rest(element_id, module_id, api_id, params)
        print(response)
        message = assemble_ws_message(element_id, module_id, api_id, response)
        ws.write_message(json.dumps(message))
    except tornado.websocket.WebSocketError:
        logger.error("Websocket connection error ")
        raise


async def apply_rest(element_id, module_id, api_id, params, body=None):
    op_type, path = api_mapper(element_id, module_id, api_id, params)
    port = get_plugin_port(module_id)
    headers = None
    url = None
    if body is not None:
        headers = {'Content-type': 'application/json'}
    # NOTE Stupid patch: remove asap
    if port:
        if 'localhost' in path:
            url = path
        else:
            url = "http://localhost:" + port + path
        if url is None:
            return url
    else:
        url = path
    if op_type == "get":
        response = await abct.get(url)
        if response and response.text:
            return response.text
    elif op_type == "post":
        response = await abct.post(url=url, data=json.dumps(body), headers=headers)
        if response and response.text:
            return response.text
    elif op_type == "put":
        response = await abct.put(url=url, data=json.dumps(body), headers=headers)
        if response and response.text:
            return response.text
    return None


def get_plugin_port(module_id):
    slice_model = store.get_slice()
    for element in slice_model['elements']:
        for ppr in element['properties']:
            if ppr['property_id'] == module_id:
                port = ppr['port']
                return port
    return None


def param_replacement(params, full_path):
    for k, v in params.items():
        full_path = full_path.replace('{'+k+'}', v[0].decode())

    return full_path


def check_module_in_slice(element_id, module_id, api_id):
    slicee = store.get_slice()
    for element in slicee['elements']:
        if element['element_id'] == element_id:
            for propertye in element['properties']:
                if propertye['property_id'] == module_id:
                    for api in propertye['allowed_api']:
                        if api['api_id'] == api_id:
                            return True
    return False


def api_mapper(element_id, module_id, api_id, params):
    #if not check_module_in_slice(element_id, module_id, api_id):
    #    return []
    modules = store.get_modules()
    real_path = ''
    op_type = None
    if module_id in modules:
        api_list = modules[module_id]['operations']
        for api in api_list:

            if api_id in api.keys():
                full_path = api[api_id]['full_path']
                real_path = param_replacement(params, full_path)
                op_type = api[api_id]['type']

    return [op_type, real_path]


# REST - Engine API map

async def register_module(module_info):
    try:
        res = cap.parse_module_api(module_info)
        store.store_module(res)
    except exp.PluginError as e:
        logger.error('Error Type: ' + e.etype + ' - Reason: ' + e.reason)


async def register_slice(slice_info):
    elements, graph = load_slice_model(slice_info)
    store.store_slice(dict(slice_info))
    store.store_graph(graph)

    slice_id = slice_info['slice_id']
    tenant_id = slice_info['tenant_id']
    reg_info = store.get_cpsr_info()
    modules = await retrieve_modules()
    payload = {"slice_id": slice_id, "slice_owner": tenant_id, "register_ip": reg_info["cpsr_ip"],
               "register_port": reg_info['cpsr_port']}
    for module_id in modules:
        await apply_rest('element_id', module_id, 'confPlugin', params={}, body=payload)


async def retrieve_modules():
    try:
        modules = store.get_modules()
    except exp.GetInfoError("ERROR No Modules found", "MODULE_NOT_FOUND"):
        raise
    return modules


async def retrieve_slice_info():
    try:
        _slice = store.get_slice()
    except exp.GetInfoError("ERROR No Slice found", "SLICE_NOT_FOUND"):
        raise
    return _slice


async def retrieve_graph():
    try:
        graph = store.get_graph()
    except exp.GetInfoError("ERROR No Graph found", "GRAPH_NOT_FOUND"):
        raise
    return graph


if __name__ == "__main__":

    with open("slice_example.json", "r") as f:
        model = f.read()

    print(load_slice_model(json.loads(model)))

    with open("plugin_test.json", "r") as f:
        plugin = f.read()

    repo = ast.ModuleStorage()
    #repo.push_on_repo(json.loads(plugin))
    print(repo.modules)
