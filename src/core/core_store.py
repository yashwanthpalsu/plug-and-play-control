

#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import threading


class ModuleStorage:
    def __init__(self):
        self.modules = {}
        self.slice = {}
        self.graph = {}
        self.callbacks = {}
        self.cpsr_info = {}
        self.lock = threading.Lock()

    def store_cpsr_info(self, info):
        self.lock.acquire()

        self.cpsr_info = info

        self.lock.release()

    def get_cpsr_info(self):

        self.lock.acquire()

        res = self.cpsr_info

        self.lock.release()
        return res

    def store_callback(self, cb_key, cb_value):
        self.lock.acquire()

        self.callbacks[cb_key] = cb_value

        self.lock.release()

    def get_callback(self, cb_key):
        cb = None
        self.lock.acquire()

        if cb_key in self.callbacks.keys():
            cb = self.callbacks[cb_key]

        self.lock.release()

        return cb

    def delete_callback(self, cb_key):
        self.lock.acquire()

        if cb_key in self.callbacks.keys():
            self.callbacks[cb_key].stop()
            del self.callbacks[cb_key]

        self.lock.release()

    def delete_all_callback(self, partial_key):
        self.lock.acquire()
        rem_list = []
        for k,v in self.callbacks.items():
            if partial_key in k:
                rem_list.append(k)

        for k in rem_list:
            self.callbacks[k].stop()
            del self.callbacks[k]

        self.lock.release()

    def store_module(self, entry):
        self.lock.acquire()

        self.modules.update(entry)

        self.lock.release()

    def get_modules(self):
        self.lock.acquire()

        mdls = self.modules

        self.lock.release()
        return mdls

    def store_slice(self, slice):
        self.lock.acquire()

        self.slice = slice

        self.lock.release()

    def get_slice(self):
        self.lock.acquire()

        slicee = self.slice

        self.lock.release()
        return slicee

    def store_graph(self, graph):
        self.lock.acquire()

        self.graph = graph

        self.lock.release()

    def get_graph(self):
        self.lock.acquire()

        graph = self.graph

        self.lock.release()
        return graph
