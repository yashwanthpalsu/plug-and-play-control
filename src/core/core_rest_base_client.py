

#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import requests
import logging
import tornado.httpclient

module_name = "P&P_BASE_REST_CLIENT"
logger = logging.getLogger(module_name)


async def get(url, headers=None,  user=None, password=None, params=None):
    auth = None

    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)

    try:
        logger.info('Executing GET to: ' + url )
        http_response = requests.request("GET", url, headers=headers, auth=auth, params=params)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error " + str(e))
        return None

    if not 199 < http_response.status_code < 300:
        logger.error("error in http_response: " + str(http_response.status_code))
        return None

    return http_response


async def post(url, data=None, headers=None, user=None, password=None):
    auth = None
    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)
    try:
        logger.info('Executing POST to: ' + url)
        http_response = requests.post(url, data, auth=auth, headers=headers)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error " + str(e))

    if not 199 < http_response.status_code < 300:
        logger.error("error in http_response: " + str(http_response.status_code))

    return http_response


async def put(url, data=None, headers=None, user=None, password=None):
    auth = None
    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)
    try:
        logger.info('Executing PUT to: ' + url)
        http_response = requests.put(url, data, auth=auth, headers=headers)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error " + str(e))

    if not 199 < http_response.status_code < 300:
        logger.error("error in http_response: " + str(http_response.status_code))

    return http_response
