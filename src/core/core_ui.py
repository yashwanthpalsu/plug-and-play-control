

#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import logging
import sys
import getopt
import tornado.web
import tornado.ioloop
import tornado.websocket
import core_engine as aet
import core_exceptions as exp

module_name = 'P&P_CORE_REST_UI'
logger = logging.getLogger(module_name)


# Endpoints
def make_osa_app():
    return tornado.web.Application([
        (r"/plug-and-play-test/monitoring/([^/]*)/([^/]*)/([^/]*)/", MonitoringHandler),
        (r"/plug-and-play-test/control/([^/]*)/([^/]*)/([^/]*)/", ControlHandler),
        (r"/plug-and-play-test/management/([^/]*)/([^/]*)/([^/]*)/", ManagementHandler),
        (r"/plug-and-play-test/websocket/([^/]*)/([^/]*)/([^/]*)/([^/]*)/", WebSocketRequestHandler),
        (r"/plug-and-play-test/pp_management/registration/slice/", RegistrationHandler),
        (r"/plug-and-play-test/pp_management/registration/cpsr-info/", CPSRHandler),
        (r"/plug-and-play-test/pp_management/get-info/(modules|slice|graph|detailed-slice)/", InfoHandler),
        (r"/plug-and-play-test/pp_management/get-element-info/([^/]*)/", ElementInfoHandler),
        (r"/plug-and-play-test/pp_management/websocket/", WebSocketHandler),
    ])


def make_plugin_internal_app():
    return tornado.web.Application([
        (r"/plug-and-play-test/pp_management/registration/module/", PluginRegistrationHandler),

    ])


def make_websocket_app():
    return tornado.web.Application([
        (r"/plug-and-play-test/websocket/", WebSocketHandler)
    ])


# Handlers
class CPSRHandler(tornado.web.RequestHandler):
    async def post(self):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        await aet.register_cpsr_info(payload)


class InfoHandler(tornado.web.RequestHandler):
    async def get(self, what):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        try:
            if what == 'modules':
                response = await aet.retrieve_modules()
            elif what == 'slice':
                response = await aet.retrieve_slice_info()
            elif what == 'graph':
                response = await aet.retrieve_graph()
            elif what == 'detailed-slice':
                response = await aet.generate_detailed_slice()
        except exp.GetInfoError as e:
            logger.error("ERROR in retrieve info - " + str(e))
            self.set_status(404, "NOT FOUND")
        else:
            self.write(response)


class ElementInfoHandler(tornado.web.RequestHandler):
    async def get(self, what):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")

        response = await aet.get_element_info(what)

        if response:

            self.write(response)
        else:
            self.set_status(404, "NOT FOUND")


class RegistrationHandler(tornado.web.RequestHandler):
    async def post(self):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        await aet.register_slice(payload)


class PluginRegistrationHandler(tornado.web.RequestHandler):
    async def post(self):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        await aet.register_module(payload)


# monitor_plugin_method = /blabla/<param_1>/<param_2>/some?x=<val1>&y=<val2>
# P&P_NBI_method: /monitor/<plugin_id>/<api_id>/?param_1=<param_1>&param_2=<param_2>&x=<val_1>&y=<val_2>
# http://127.0.0.1:60001/plug-and-play-test/monitoring/test_plugin_1/getMeas/?indexType=QOS


class MonitoringHandler(tornado.web.RequestHandler):
    async def get(self, element_id, module_id, api_id):
        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Monitoring')
        print(self.request.uri)
        print(self.request.query_arguments)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)
        else:
            self.set_status(404, "NOT FOUND")

    async def put(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Monitoring')
        print(self.request)
        payload = tornado.escape.json_decode(self.request.body)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments, payload)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)

    async def post(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Monitoring')
        print(self.request)
        payload = tornado.escape.json_decode(self.request.body)
        print(payload)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments, payload)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)


class ControlHandler(tornado.web.RequestHandler):
    async def get(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Control')
        print(self.request.uri)
        print(self.request.query_arguments)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)
        else:
            self.set_status(404, "NOT FOUND")

    async def post(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Control')
        print(self.request)
        payload = tornado.escape.json_decode(self.request.body)
        print(payload)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments, payload)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)

    async def put(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Control')
        print(self.request)
        payload = tornado.escape.json_decode(self.request.body)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments, payload)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)


class ManagementHandler(tornado.web.RequestHandler):
    async def get(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Management')
        print(self.request.uri)
        print(self.request.query_arguments)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)
        else:
            self.set_status(404, "NOT FOUND")

    async def post(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Management')
        print(self.request)
        payload = tornado.escape.json_decode(self.request.body)
        print(payload)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments, payload)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)

    async def put(self, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('Management')
        print(self.request)
        payload = tornado.escape.json_decode(self.request.body)
        response = await aet.apply_rest(element_id, module_id, api_id, self.request.query_arguments, payload)
        print(response)
        if response:
            self.set_header("Content-Type", "application/json")
            self.write(response)


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    ws = {}

    def check_origin(self, origin):
        # self.set_header("Access-Control-Allow-Origin", "*")
        return True

    def open(self):
        # self.set_header("Access-Control-Allow-Origin", "*")
        logger.debug("Websocket Opened")
        type(self).ws[id(self)] = self   # websocket object assignment
        self.write_message(str(id(self)))

    def on_close(self):
        # self.set_header("Access-Control-Allow-Origin", "*")
        logger.debug("Received WS CLOSE Request: Removing all flows")
        aet.remove_all_callbacks(str(id(self)))
        del type(self).ws[id(self)]


class WebSocketRequestHandler(tornado.web.RequestHandler):
    async def put(self, ws_id, element_id, module_id, api_id):
        self.set_header("Access-Control-Allow-Origin", "*")
        logger.debug('WebSocket Request')
        logger.debug(self.request)

        if int(ws_id) in WebSocketHandler.ws.keys():
            cb = tornado.ioloop.PeriodicCallback(lambda: aet.websocket_routine(WebSocketHandler.ws[int(ws_id)], element_id, module_id, api_id, self.request.query_arguments), 1000)
            cb.start()
            # put cb somewhere
            cb_key = "{}_{}_{}".format(ws_id, element_id, module_id, api_id)

            aet.store_callback(cb_key, cb)
            self.set_status(201, "CREATED")

        else:
            self.set_status(404, "NOT FOUND")

    async def delete(self, ws_id, element_id, module_id, api_id):
        # cb_key = ws_id + "_" + element_id + "_" + module_id + "_" + api_id
        cb_key = "{}_{}_{}".format(ws_id, element_id, module_id, api_id)
        logger.debug('Removing ' + cb_key)
        aet.remove_callback(cb_key)


# Main
def main(argv):
    # DEFAULTS
    osa_port = 60002
    plugin_port = 60001
    cpsr_ip = "127.0.0.1"
    cpsr_port = "8080"
    slice_id = ""

    try:
        opts, args = getopt.getopt(argv, "P:p:C:c:i", ["oport", "pport", "cpsrip", "cpsrport", "sliceid"])

        for opt, arg in opts:
            if opt in ('-P', '-oport'):
                osa_port = arg
            elif opt in ('-p', '-pport'):
                plugin_port = arg
            elif opt in ('-C', '-cpsrip'):
                cpsr_ip = arg
            elif opt in ('-c', '-cpsrport'):
                cpsr_port = arg
            elif opt in ('-i', '-sliceid'):
                slice_id = arg

        osa_app = make_osa_app()
        osa_app.listen(osa_port)
        plugin_app = make_plugin_internal_app()
        plugin_app.listen(plugin_port)

        print("Starting REST UI")
        tornado.ioloop.IOLoop.instance().start()
    except getopt.GetoptError:
        logger.error("Error in parsing command line arguments")
    except KeyboardInterrupt:
        logger.error("Keyboard Interrupt caught: closing.")
    finally:
        exit(0)


if __name__ == '__main__':
    main(sys.argv[1:])
