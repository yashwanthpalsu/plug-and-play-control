## Plug and Play Control
Customized Control and Management plane for SliceNet Verticals

### Overview
The SliceNet Plug & Play control framework aims at offering verticals an isolated control
environment, including when required and applicable also management and monitoring features,
specific per slice instance that can be activated on-demand upon new slice instances provisioning.

The customization of the functionalities offered toward the Vertical is realized by deploy the P&P Control 
as a set of Docker containers (plugins) that provide single feature to be exposed. An additional container 
encapsulating the Core of the frame. All of the plugins will connect themselves to the Core and the
Core will expose via a REST UI all the features collected by the registered plugins.

### Usage
Under /src/core is available the source code for the Core of the framework while the plugins are 
available under /plugins. 

All the containerizable elements have their own specific docker file. For a correct usage, [Kubernetes] is required. 
Once the images have been created, they can be deployed in the same Kubernetes POD: the plugins will register to the Core automatically.

#### Operation Examples
##### Check plugin registration
GET: ```http://<kubernetes-ip>:<service-port>/plug-and-play-test/pp_management/get-info/modules/```

```json
{
    "qos-e2e-plugin": {
        "base_path": "/localhost:65010",
        "operations": [
            {
                "confPlugin": {
                    "type": "post",
                    "full_path": "http://localhost:65010/cpsr-info",
                    "responses": {
                        "200": {
                            "description": "successful operation"
                        }
                    },
                    "parameters": [
                        {}
                    ]
                }
            },
            {
                "get_e2e_throughput_info": {
                    "type": "get",
                    "full_path": "http://localhost:65010/qos_plugin/get_e2e_throughput_info",
                    "responses": {
                        "200": {
                            "description": "OK"
                        }
                    },
                    "parameters": [
                        {}
                    ]
                }
            }
        ]
    }
}
```
##### Configure Slice Vertical View
For linking correctly the API interface provided by the plugins to the Core, a special JSON
must be submitted through the P&P Control interface.

POST ```http://<kubernetes-ip>:<service-port>/plug-and-play-test/pp_management/registration/slice/``` 

```json
{
    "slice_id": "bf5cbb04-345c-479f-8511-f6f01b2b822d",
    "domain_type": "single",
    "graph": [
        {
            "adjacent_elements": [
                {
                    "node_id": "Health-001",
                    "link_id": "VLINK-001"
                }
            ],
            "node_id": "UE-001"
        }
    ],
    "tenant_id": "dell",
    "elements": [
        {
            "location": "e2e_service",
            "domain_id": "domain-001",
            "element_name": "eHealt_service",
            "element_id": "EHEALTH-001",
            "type": "node",
            "properties": [
                {
                    "type": "plugin",
                    "port": "65005",
                    "family": "monitoring",
                    "allowed_api": [
                        {
                            "api_id": "get_e2e_throughput_info", 
                            "forwarding": "direct"
                        }
                    ],
                    "property_id": "qos-e2e-plugin"
                }
            ],
            "family": "",
            "level": "Service"
        },
        {
            "location": "e2e_service",
            "domain_id": "domain-001",
            "element_name": "Ambulance-1",
            "element_id": "UE-001",
            "type": "node",
            "properties": [],
            "family": "UE",
            "level": "Service"
        },
        {
            "location": "e2e_service",
            "domain_id": "domain-001",
            "element_name": "Hospital",
            "element_id": "Health-001",
            "type": "node",
            "properties": [],
            "family": "CORE",
            "level": "Service"
        },
        {
            "location": "e2e_service",
            "domain_id": "domain-001",
            "element_name": "5G-link",
            "element_id": "VLINK-001",
            "type": "link",
            "properties": [],
            "family": "",
            "level": "Service"
        }
    ]
}
```
##### Retrieving the plugin features mapped on Slice View  
In elements -> properties -> allowed_api -> endpoint is visible the new endpoint in the Core
of the API belonging to the plugin interface

POST ```http://<kubernetes-ip>:<service-port>/plug-and-play-test/pp_management/registration/slice/``` 

```json
{
    "slice_id": "bf5cbb04-345c-479f-8511-f6f01b2b822d",
    "domain_type": "single",
    "graph": [
        {
            "adjacent_elements": [
                {
                    "node_id": "Health-001",
                    "link_id": "VLINK-001"
                }
            ],
            "node_id": "UE-001"
        }
    ],
    "tenant_id": "dell",
    "elements": [
        {
            "location": "e2e_service",
            "domain_id": "domain-001",
            "element_name": "eHealt_service",
            "element_id": "EHEALTH-001",
            "type": "node",
            "properties": [
                {
                    "type": "plugin",
                    "port": "65005",
                    "family": "monitoring",
                    "allowed_api": [
                        {
                            "api_id": "get_e2e_throughput_info",
                            "operation_type": "get",
                            "endpoint": "/plug-and-play-test/monitoring/EHEALTH-001/qos-e2e-plugin/get_e2e_throughput_info/",
                            "parameters": [
                                {}
                            ],
                            "forwarding": "direct"
                        }
                    ],
                    "property_id": "qos-e2e-plugin"
                }
            ],
            "family": "",
            "level": "Service"
        },
       ...
    ]
}
```
