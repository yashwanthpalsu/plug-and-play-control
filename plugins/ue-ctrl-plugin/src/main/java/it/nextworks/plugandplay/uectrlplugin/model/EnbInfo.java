package it.nextworks.plugandplay.uectrlplugin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

    /**
     * Created by json2java on 22/06/17.
     * json2java author: Marco Capitani (m.capitani AT nextworks DOT it)
     */

public class EnbInfo {

    public EnbInfo() {

    }

    @JsonProperty("dlrb")
    private int dlrb;

    @JsonProperty("dlrb")
    public int getDlrb() {
        return dlrb;
    }

    @JsonProperty("dlrb")
    private void setDlrb(int dlrb) {;
        this.dlrb = dlrb;
    }

    @JsonProperty("dlmaxmcs")
    private int dlmaxmcs;

    @JsonProperty("dlmaxmcs")
    public int getDlmaxmcs() {
        return dlmaxmcs;
    }

    @JsonProperty("dlmaxmcs")
    private void setDlmaxmcs(int dlmaxmcs) {;
        this.dlmaxmcs = dlmaxmcs;
    }

    @JsonProperty("id")
    private int id;

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    private void setId(int id) {;
        this.id = id;
    }

    @JsonProperty("ulrb")
    private int ulrb;

    @JsonProperty("ulrb")
    public int getUlrb() {
        return ulrb;
    }

    @JsonProperty("ulrb")
    private void setUlrb(int ulrb) {;
        this.ulrb = ulrb;
    }

    @JsonProperty("ulmaxmcs")
    private int ulmaxmcs;

    @JsonProperty("ulmaxmcs")
    public int getUlmaxmcs() {
        return ulmaxmcs;
    }

    @JsonProperty("ulmaxmcs")
    private void setUlmaxmcs(int ulmaxmcs) {;
        this.ulmaxmcs = ulmaxmcs;
    }

}