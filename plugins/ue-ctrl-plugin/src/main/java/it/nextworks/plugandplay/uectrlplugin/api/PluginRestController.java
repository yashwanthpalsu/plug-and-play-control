package it.nextworks.plugandplay.uectrlplugin.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import it.nextworks.plugandplay.uectrlplugin.engine.PluginEngine;
import it.nextworks.plugandplay.uectrlplugin.model.Ue;
import it.nextworks.plugandplay.uectrlplugin.model.UeInfo;
import io.swagger.annotations.ApiResponse;

@CrossOrigin
@RestController
@RequestMapping(value="/plugin/control")
public class PluginRestController {

	private Logger log = LoggerFactory.getLogger(PluginRestController.class);
	
	@Autowired
	private PluginEngine pluginEngine;
	
	@RequestMapping(value="/ues", method=RequestMethod.POST)
	@ApiOperation(value = "add new UE to slice", nickname = "add-New-UE-to-slice")
		@ApiResponses(value = { 
	        @ApiResponse(code = 201, message = "Created"),
	        @ApiResponse(code = 400, message = "Bad Request")}) 
	public ResponseEntity<String> setNewUe(@RequestBody Ue request) throws Exception {

		log.debug("Received new request for set UE " + request.getImsi());

		try {
			
			pluginEngine.setUe(request);
			
		} catch (Exception e) {
			log.error("Cannot set new UE: " + e.getMessage());
			return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("", HttpStatus.CREATED);
	}

	@RequestMapping(value="/ues/{ueId}", method=RequestMethod.GET)
	@ApiOperation(value = "get UE info", nickname = "get-UE-info")
	@ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = UeInfo.class),
        @ApiResponse(code = 404, message = "Not Found")})
	public ResponseEntity<UeInfo> getUe(@PathVariable String ueId) throws Exception {

		log.debug("Received new request for getting info on UE " + ueId);

		try {
			
			UeInfo ue = pluginEngine.getUeInfo(ueId);
			
			return new ResponseEntity<UeInfo>(ue, HttpStatus.OK);
			
		} catch (Exception e) {
			log.error("Cannot get UE info: " + e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
}