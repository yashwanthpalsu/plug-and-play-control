package it.nextworks.plugandplay.uectrlplugin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

    /**
     * Created by json2java on 22/06/17.
     * json2java author: Marco Capitani (m.capitani AT nextworks DOT it)
     */

public class UeResp {

    public UeResp() {

    }

    @JsonProperty("enb_info")
    private EnbInfo enbInfo;

    @JsonProperty("enb_info")
    public EnbInfo getEnbInfo() {
        return enbInfo;
    }

    @JsonProperty("enb_info")
    private void setEnbInfo(EnbInfo enbInfo) {;
        this.enbInfo = enbInfo;
    }

    @JsonProperty("rnti")
    private int rnti;

    @JsonProperty("rnti")
    public int getRnti() {
        return rnti;
    }

    @JsonProperty("rnti")
    private void setRnti(int rnti) {;
        this.rnti = rnti;
    }

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("ip_address")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ip_address")
    private void setIpAddress(String ipAddress) {;
        this.ipAddress = ipAddress;
    }

    @JsonProperty("phr")
    private int phr;

    @JsonProperty("phr")
    public int getPhr() {
        return phr;
    }

    @JsonProperty("phr")
    private void setPhr(int phr) {;
        this.phr = phr;
    }

    @JsonProperty("imsi")
    private String imsi;

    @JsonProperty("imsi")
    public String getImsi() {
        return imsi;
    }

    @JsonProperty("imsi")
    private void setImsi(String imsi) {;
        this.imsi = imsi;
    }

    @JsonProperty("wbCQI")
    private int wbcqi;

    @JsonProperty("wbCQI")
    public int getWbcqi() {
        return wbcqi;
    }

    @JsonProperty("wbCQI")
    private void setWbcqi(int wbcqi) {;
        this.wbcqi = wbcqi;
    }

}