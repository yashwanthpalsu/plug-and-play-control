package it.nextworks.plugandplay.uectrlplugin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CPServiceLink {

	private String uri;

	public CPServiceLink() {

	}

	@JsonProperty("uri")
	public String getUri() {
		return uri;
	}
	
	
}
