package it.nextworks.plugandplay.uectrlplugin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Ue {

	private String imsi;
	
	public Ue() {
	}
	
	public Ue(String imsi) {
		this.imsi = imsi;
	}

	@JsonProperty("imsi")
	public String getImsi() {
		return imsi;
	}
	
}
