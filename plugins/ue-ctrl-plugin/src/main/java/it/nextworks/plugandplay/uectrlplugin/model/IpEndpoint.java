package it.nextworks.plugandplay.uectrlplugin.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class IpEndpoint {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String ipv4Address;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String ipv6Address;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String ipv6Prefix;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String transport;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private int port;
	
	public IpEndpoint() {
		
	}

	@JsonProperty("ipv4Address")
	public String getIpv4Address() {
		return ipv4Address;
	}

	@JsonProperty("ipv6Address")
	public String getIpv6Address() {
		return ipv6Address;
	}

	@JsonProperty("ipv6Prefix")
	public String getIpv6Prefix() {
		return ipv6Prefix;
	}

	@JsonProperty("transport")
	public String getTransport() {
		return transport;
	}

	@JsonProperty("port")
	public int getPort() {
		return port;
	}
	
	
}
