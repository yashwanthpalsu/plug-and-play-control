package it.nextworks.plugandplay.uectrlplugin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import it.nextworks.plugandplay.uectrlplugin.engine.PluginEngine;

@SpringBootApplication
public class UeCtrlPluginApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(UeCtrlPluginApplication.class, args);
	}
}
