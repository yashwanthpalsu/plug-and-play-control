package it.nextworks.plugandplay.uectrlplugin.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CpsrDiscoveryResponse {
	
	private List<CPServiceLink> cpss = new ArrayList<>();

	@JsonProperty("cpss")
	public List<CPServiceLink> getCpss() {
		return cpss;
	}
}
