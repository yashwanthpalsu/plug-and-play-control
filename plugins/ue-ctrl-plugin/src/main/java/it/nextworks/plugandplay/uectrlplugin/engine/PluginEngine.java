package it.nextworks.plugandplay.uectrlplugin.engine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.nextworks.plugandplay.uectrlplugin.model.CPService;
import it.nextworks.plugandplay.uectrlplugin.model.CPServiceLink;
import it.nextworks.plugandplay.uectrlplugin.model.CpsrDiscoveryResponse;
import it.nextworks.plugandplay.uectrlplugin.model.PluginConfig;
import it.nextworks.plugandplay.uectrlplugin.model.Ue;
import it.nextworks.plugandplay.uectrlplugin.model.UeInfo;
import it.nextworks.plugandplay.uectrlplugin.model.UeResp;

@Service
public class PluginEngine {
	
	private Logger log = LoggerFactory.getLogger(PluginEngine.class);

	private RestTemplate restTemplate;
	
	private PluginConfig config;
	
	private boolean isConfigured;
	
	@Value("${swagger.doc.uri:/v2/api-docs}")
	private String swaggerDocUri;
		
	@Value("${server.address}")
	private String swaggerIp;
	
	@Value("${server.port}")
	private String swaggerPort;
	
	@Value("${plugandplay.registration.address:localhost}")
	private String registrationIp;
	
	@Value("${plugandplay.registration.port:60001}")
	private String registrationPort;
	
	@Value("${plugandplay.registration.uri:/plug-and-play-test/pp_management/registration/module/}")
	private String registrationUri;
	
	@Value("${plugandplay.cpsr.uri}")
	private String cpsrUri;
	
	@Value("${plugandplay.ue.mgmt.uri:/ues/}")
	private String cpAppUri;
	
	@Value("${plugandplay.test.mode:false}")
	private boolean isTest;
	
	public PluginEngine() {
		this.restTemplate = new RestTemplate();
		this.config = new PluginConfig();
		this.isConfigured = false;
	}
	
	@EventListener(ApplicationReadyEvent.class)
	private void init() {
		try {
			log.info("Plugin is up and running - going to register to core service");
			
			String json = getOpenApiDoc();
			
			connectToPlugAndPlay(json);
			
			log.info("Registration to plug-and-play successful !!");
			
		} catch (Exception e) {
			log.error("Cannot connect to plug-and-play: " + e.getMessage());
		}
	}
	
	public UeInfo getUeInfo(String ueId) throws Exception {
		
		log.info("Processing getUeInfo for UE " + ueId);
		
		if (isTest) {
			log.info("TEST - OK");
			return new UeInfo("123456789", "10.10.0.1", "eNB-1", "ATTACHED");
		}
		
		//go to cpsr to retrieve the proper service to invoke
		if (!isConfigured) {
			throw new Exception("Plugin is not configured");
		}
		
		String appUrl = retrieveCpAppEndpoint() + cpAppUri+ "/get_ue_info/{slice-id}/{imsi}";
		Map<String, Object> params = new HashMap<>();
		params.put("slice-id", config.getSliceId());
		params.put("imsi", ueId);
	
		log.info("Going to invoke UE CP app");
		
		ResponseEntity<UeResp> httpResponse = restTemplate.getForEntity(appUrl, UeResp.class, params);
		
		UeInfo ue = null;
		
		if (httpResponse.getStatusCode() != HttpStatus.OK) {
			//log.error("Cannot get openAPI (code " + httpResponse.getStatusCode() + ")");
			//throw new Exception("Cannot get Ue info (code " + httpResponse.getStatusCode() + ")");
			log.info("UE seems not to be attached");

			ue = new UeInfo(ueId, "",
					  "enb-?", "DETACHED");			
		} else {
		
			UeResp cpUe = httpResponse.getBody();
			log.info("UE succesfully retrieved - " + cpUe.getIpAddress());
			
			ue = createUeInfo(cpUe);
		}
		
		return ue;
	}
	
	private UeInfo createUeInfo(UeResp cpInfo) {
		
		if (cpInfo.getEnbInfo() != null &&
			cpInfo.getIpAddress() != null &&
			!cpInfo.getIpAddress().isEmpty()) {
			return new UeInfo(cpInfo.getImsi(), cpInfo.getIpAddress(),
					  "enb-" + cpInfo.getEnbInfo().getId(), "ATTACHED");
		} else {
			return new UeInfo(cpInfo.getImsi(), "",
						  "enb-?", "DETACHED");
		}
	}
	
	public void setUe(Ue ue) throws Exception {
		
		log.info("Received setUe for Ue: " + ue.getImsi());
		
		if (isTest) {
			log.info("TEST - New UE successfully set");
			return;
		}
		
		//go to cpsr to retrieve the proper service to invoke
		if (!isConfigured) {
			throw new Exception("Plugin is not configured");
		}
		
		String appUrl = retrieveCpAppEndpoint() + cpAppUri + "/set_ue/{slice-id}/{imsi}";
		Map<String, Object> params = new HashMap<>();
		params.put("slice-id", config.getSliceId());
		params.put("imsi", ue.getImsi());
		
		log.info("Going to contact CP app to add new UE to the slice " + this.config.getSliceId());
		
		ResponseEntity<String> httpResponse = restTemplate.postForEntity(appUrl, "", String.class, params);
		
		if (httpResponse.getStatusCode() != HttpStatus.OK &&
			httpResponse.getStatusCode() != HttpStatus.ACCEPTED) {
			//log.error("Cannot get openAPI (code " + httpResponse.getStatusCode() + ")");
			throw new Exception("Cannot add UE to slice (code " + httpResponse.getStatusCode() + ")");
		}	
		
		log.info("New UE successfully set");
	}
	
	private String retrieveCpAppEndpoint() throws Exception {
		
		log.info("Going to retrieve proper UE-CP from CPSR");
		
		String cpsrUrl = "http://" + config.getRegisterIp() + ":" + config.getRegisterPort() +
				cpsrUri + "?CPS-Type={type}&sliceId={id}" ;

		Map<String, Object> params = new HashMap<>();
		params.put("type", "UE-CP");
		params.put("id", config.getSliceId());
		ResponseEntity<CpsrDiscoveryResponse> httpResponse = restTemplate.getForEntity(cpsrUrl, CpsrDiscoveryResponse.class, params);
		
		if (httpResponse.getStatusCode() != HttpStatus.OK) {
			//log.error("Cannot get openAPI (code " + httpResponse.getStatusCode() + ")");
			throw new Exception("Cannot retrieve UE CP app (code " + httpResponse.getStatusCode() + ")");
		}
		
		if (httpResponse.getBody() == null ||
			httpResponse.getBody().getCpss().size() == 0) {
			throw new Exception("CP Service IP endpoints not available");
		}
		//
		log.info("UE-CP app retrieved is: " + httpResponse.getBody().getCpss().get(0).getUri());
		//return CP service ip, port and api prefix
		return httpResponse.getBody().getCpss().get(0).getUri();
	}
	
	public void setConfig(PluginConfig config) throws Exception {
		this.config = config;
		this.isConfigured = true;
	}
	
	public String getOpenApiDoc() throws Exception {
		
		log.info("Retrieving openAPI documentation");	

		String swaggerUrl = "http://" + swaggerIp + ":" + swaggerPort + swaggerDocUri;
		
		ResponseEntity<String> httpResponse = restTemplate.getForEntity(swaggerUrl, String.class);
		
		if (httpResponse.getStatusCode() != HttpStatus.OK) {
			//log.error("Cannot get openAPI (code " + httpResponse.getStatusCode() + ")");
			throw new Exception("Cannot get openAPI (code " + httpResponse.getStatusCode() + ")");
		}
		
		String body = httpResponse.getBody();
		log.info("Plugin OpenAPI is: \n" + body);
		
		return body;
	}
	
	public void connectToPlugAndPlay(String openApiJson) throws Exception {
		
		String registrationUrl = "http://" + registrationIp + ":" + registrationPort + registrationUri;
		
		log.info("Going to register to plug-and-play - url: " + registrationUrl);
		
		ResponseEntity<String> httpResponse = restTemplate.postForEntity(registrationUrl, openApiJson, String.class);
		
		if (httpResponse.getStatusCode() != HttpStatus.OK &&
			httpResponse.getStatusCode() != HttpStatus.ACCEPTED) {
			//log.error("Cannot get openAPI (code " + httpResponse.getStatusCode() + ")");
			throw new Exception("Cannot register openAPI (code " + httpResponse.getStatusCode() + ")");
		}	
	}
}
