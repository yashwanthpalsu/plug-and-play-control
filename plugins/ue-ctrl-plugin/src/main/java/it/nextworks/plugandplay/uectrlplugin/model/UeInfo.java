package it.nextworks.plugandplay.uectrlplugin.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UeInfo {

	private String imsi;
	private String ip;
	private String location;
	private String status;
	
	public UeInfo() {
	}
	
	public UeInfo(String imsi, String ip, String location, String status) {
		this.imsi = imsi;
		this.ip = ip;
		this.location = location;
		this.status = status;
	}

	@JsonProperty("imsi")
	public String getImsi() {
		return imsi;
	}
	
	@JsonProperty("ip")
	public String getIp() {
		return ip;
	}
	
	@JsonProperty("location")
	public String getLocation() {
		return location;
	}
	
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class UeInfo {\n");

		sb.append("    imsi: ").append(toIndentedString(imsi)).append("\n");
		sb.append("    ip: ").append(toIndentedString(ip)).append("\n");
		sb.append("    location: ").append(toIndentedString(location)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
	
}
