package it.nextworks.plugandplay.uectrlplugin.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CPService {

	private String serviceInstanceId;
	
	private String serviceName;
	
	private String version;
	
	private String schema;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String fqdn;
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<IpEndpoint> ipEndpoints = new ArrayList<>();
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String apiPrefix;
	
	public CPService() {
		
	}

	@JsonProperty("serviceInstanceId")
	public String getServiceInstanceId() {
		return serviceInstanceId;
	}

	@JsonProperty("serviceName")
	public String getServiceName() {
		return serviceName;
	}

	@JsonProperty("version")
	public String getVersion() {
		return version;
	}

	@JsonProperty("schema")
	public String getSchema() {
		return schema;
	}

	@JsonProperty("fqdn")
	public String getFqdn() {
		return fqdn;
	}

	@JsonProperty("ipEndPoints")
	public List<IpEndpoint> getIpEndpoints() {
		return ipEndpoints;
	}

	@JsonProperty("apiPrefix")
	public String getApiPrefix() {
		return apiPrefix;
	}


}
