package it.nextworks.plugandplay.uectrlplugin.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PluginConfig {

	private String sliceId;
	private String sliceOwner;
	private String registerIp;
	private String registerPort;	
	
	public PluginConfig() {
		this.sliceId = null;
		this.sliceOwner = null;
		this.registerIp = null;
		this.registerPort = null;
	}
	
	@JsonProperty("slice_id")
	public String getSliceId() {
		return sliceId;
	}
	
	@JsonProperty("slice_owner")
	public String getSliceOwner() {
		return sliceOwner;
	}
	
	@JsonProperty("register_ip")
	public String getRegisterIp() {
		return registerIp;
	}
	
	@JsonProperty("register_port")
	public String getRegisterPort() {
		return registerPort;
	}
	
    
	
}
