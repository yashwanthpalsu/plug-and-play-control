#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import time
from kafka import KafkaProducer


def produce():
    producer = KafkaProducer(bootstrap_servers='172.17.0.2:9092')
    for _ in range(200):
        future = producer.send('SkyDiveStats',
                               '{"Values": [{"LastTime": 1560489292, "ulPercentage": 100, "RTT": 0.065415, "Bid": "", '
                               '"ulSliceId": 0, "dl_freq": 2685, "AddB": "10.8.10.203", "AddA": "10.8.255.33", '
                               '"ul_freq": 2565, "ul_bandwidth": 50, "imsi": "208920100001102", "eNB_id": "234946560", '
                               '"A": "10.8.255.33", "B": "10.8.10.203", "portB": 5201, "portA": 54732, "ABthr": 13133.1, '
                               '"band": 7, "BAthr": 303.25, "Aid": "", "ue_ip": "172.16.0.7", "dlPercentage": 100, '
                               '"enb_ip": "10.8.255.5", "dl_bandwidth": 50, "APP": "TCP", "core_slice_id": 0, '
                               '"rnti": 51557, "dlSliceId": 0}]}'.encode())
        result = future.get(timeout=60)
        time.sleep(1)
        print(_)


if __name__ == '__main__':
    produce()
