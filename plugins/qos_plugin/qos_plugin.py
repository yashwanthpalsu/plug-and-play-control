
#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import json
import tornado.web
import tornado.ioloop
import urllib.parse
import random
import requests
import logging
import time

logging.basicConfig(format='[%(name)s] %(asctime)s - %(message)s', level=logging.DEBUG)
module_name = "QOS_PLUGIN"
logger = logging.getLogger(module_name)


ConfigInfo = dict()
ConfigInfo['slice_id'] = "0"
ConfigInfo['register_ip'] = "http://localhost"
ConfigInfo['register_port'] = "62001"


def make_app():
    return tornado.web.Application([
        (r"/qos_plugin/get_avg_throughput_info", InfoHandler),
        (r"/cpsr-info", CPSRHandler),
    ])


class InfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        # Retrieve service info
        url = get_service_info('DB-CP')
        res = get_value_from_cassandra(url, ConfigInfo['slice_id'])
        avg_response = elaborate_avg_data(json.loads(res.text), ConfigInfo['slice_id'])
        print(avg_response)
        url = get_service_info('RAN-CP')
        res = get_values_from_flexran(url, ConfigInfo['slice_id'])
        flexran_values = elaborate_current_data(json.loads(res.text), ConfigInfo['slice_id'])
        print(flexran_values)
        response = dict()
        response['slice_id'] = ConfigInfo['slice_id']
        response['timestamp'] = int(time.time())
        response['current_values'] = flexran_values
        response['average_values'] = avg_response

        self.write(response)


class CPSRHandler(tornado.web.RequestHandler):
    def post(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        logger.info(payload['register_ip'])
        ConfigInfo['slice_id'] = payload['slice_id']
        if 'http://' not in payload['register_ip']:
            ConfigInfo['register_ip'] = 'http://' + payload['register_ip']
        else:
            ConfigInfo['register_ip'] = payload['register_ip']
        ConfigInfo['register_port'] = payload['register_port']


def get_service_info(cps_type):
    url = ConfigInfo['register_ip'] + ':' + ConfigInfo[
        'register_port'] + '/ctrlplane/cpsr/v1/cps-instances?CPS-Type=' + \
          cps_type + '&sliceId=' + ConfigInfo['slice_id']
    logger.info('URL: ' + url)
    response = get(url=url)
    if response is not None:
        got_url = json.loads(response.text)['cpss'][0]['uri']
        if 'http://' not in got_url:
            got_url = 'http://' + got_url
        return got_url
    else:
        logger.error('ERROR in retrieving information from CPSR at ' + ConfigInfo['register_ip'] + ":"
                     + ConfigInfo['register_port'])
        return None


def get_values_from_flexran(url, slice_id):
    url += "/slice/" + slice_id
    response = get(url=url)
    return response


#"http://127.0.0.1:64005/cassandra_handler"
def get_value_from_cassandra(url, slice_id):
    url += "/get_slice_ue_info/"+slice_id
    response = get(url=url)
    return response


def elaborate_current_data(data, slice_id):
    ue = []
    ultbs_sum = 0
    dltbs_sum = 0
    current_value = dict()
    for entry in data[slice_id]['ran']:
        ultbs_sum += entry['ultbs']
        dltbs_sum += entry['dltbs']
        ue.append({'imsi': entry['imsi'], 'ul_throughput': entry['ultbs'] / 125,
                   'dl_throughput': entry["dltbs"] / 125})
    current_value['slice_ul_throughput'] = ultbs_sum/125
    current_value['slice_dl_throughput'] = dltbs_sum/ 125
    current_value['UEs'] = ue
    current_value['flows'] = data[slice_id]['flows']['flows']
    return current_value


"""def elaborate_avg_data(data, slice_id):
    ue = []
    ultbs_sum = 0
    dltbs_sum = 0
    avg_data = dict()
    for entry in data[slice_id]:
        ultbs_sum += entry['system.sum(ultbs)']
        dltbs_sum += entry['system.sum(dltbs)']
        ue.append({'imsi': entry['imsi'], 'avg_ul_throughput': entry['system.avg(dltbs)']/1000,
                   'avg_dl_throughput': entry["system.avg(ultbs)"]/1000})
    count = data[slice_id][0]["system.count(dltbs)"]
    # NOTE tbs is nbit per tti. tti is 1 ms. In Flexran context, ue_tbses are expessed in BYTE. To obtain Mbps: tbs x 8 x 1000 / 10000000 = tbs/125
    avg_data['slice_avg_ul_throughput'] = ultbs_sum/count/1000
    avg_data['slice_avg_dl_throughput'] = dltbs_sum / count / 1000
    avg_data['UEs'] = ue
    return avg_data
"""


def elaborate_avg_data(data, slice_id):
    ue = []
    ultbs_sum = 0
    dltbs_sum = 0
    avg_data = dict()
    for entry in data[slice_id]:
        ultbs_sum += entry['system.sum(ultbs)']
        dltbs_sum += entry['system.sum(dltbs)']
        ue.append({'imsi': entry['imsi'], 'avg_ul_throughput': entry['system.avg(ultbs)']/125,
                   'avg_dl_throughput': entry["system.avg(dltbs)"]/125})
    count = 0
    for c in data[slice_id]:
        if count < c["system.count(dltbs)"]:
            count = c["system.count(dltbs)"]
    # NOTE tbs is nbit per tti. tti is 1 ms. In Flexran context, ue_tbses are expessed in BYTE.
    # NOTE To obtain Mbps: tbs x 8 x 1000 / 10000000 = tbs/125
    avg_data['slice_avg_ul_throughput'] = ultbs_sum/count/125
    avg_data['slice_avg_dl_throughput'] = dltbs_sum/count/125
    avg_data['UEs'] = ue
    return avg_data


def get(url, headers=None,  user=None, password=None, params=None):
    auth = None

    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)

    try:
        http_response = requests.request("GET", url, headers=headers, auth=auth, params=params)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error", e)
        return None

    if http_response.status_code > 299:
        logger.error("GET ERROR")
        return None

    return http_response


def post(url, data=None, headers=None, user=None, password=None):
    auth = None
    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)
    try:
        http_response = requests.post(url, data, auth=auth, headers=headers)
    except requests.exceptions.RequestException as e:
        logger.error("Error in request", e)

    if http_response.status_code > 299:
        logger.error("error in http_response")
        raise requests.exceptions.RequestException
    return http_response


def load_api():
    with open("qos_plugin.json", "r") as f:
        json_api = json.loads(f.read())

    return json_api


def main():
    app = make_app()
    app.listen(65007)
    try:
        logger.debug('Loading API from file')
        api = load_api()
        logger.debug("Trying registration to P&P Core")
        #post('http://127.0.0.1:60001/plug-and-play-test/pp_management/registration/module/', data=json.dumps(api))
        post('http://localhost:60001/plug-and-play-test/pp_management/registration/module/', data = json.dumps(api))
        logger.debug("Starting REST UI")
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        logger.error("Keyboard Interrupt caught: closing.")
    except requests.exceptions.RequestException:
        logger.error("Error in registration to P&P Core")
    except IOError as e:
        logger.error("Cannot open API file" + str(e))
    finally:
        exit(0)


if __name__ == "__main__":
    main()
